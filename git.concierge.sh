#!/bin/bash

Timeout=10 # 10 secondes


function checkClones() {
	gitlist=($(ls -a /home/jean-morgane/git | grep .git))
	clonelist=($(ls -a -R /home/jean-morgane/public_html -R | grep /.git:))
	gitname=()
	for g in ${gitlist[@]}
	do
		gitname+=(${g/'.git'/''})
	done
	#echo ${gitname[@]}
	index=0
	for c in ${clonelist[@]}
	do
		echo
		clone=${c::-6}
		clonename=(${clone/'/home/jean-morgane/public_html/'/''})
		gitname=${gitname[index]}
		echo $gitname $clonename
		if [ "$gitname" == "$clonename" ]
		then
			echo true
			cd $clonename
			remote=$(env -i git remote -v | grep gitlab)
			if [ "$remote" == "" ]
			then
				echo no gitlab remote
			fi
			cd ..
		else
			cd /home/jean-morgane/public_html
			echo ${gitlist[index]}
			env -i git clone ../git/${gitlist[index]}
			#checkClones
		fi
		let "index++"
	done
}
checkClones
#function add_gitlab_remote() {
	#for d in "${dirlist[@]}"
	#do
		#d_path=${d::-6}
		#cd $d_path
		#echo 
		#echo '#add gitlab remote to '$d_path
		#echo 
		#gitname=${d_path/'/home/jean-morgane/public_html/'/''}
		#env -i git remote add gitlab 'git@gitlab.com:design-non-garanti/'$gitname'.git'
	#done
#}
#
#add_gitlab_remote
#
#function timeout_monitor() {
	#sleep $Timeout
	#for d in "${dirlist[@]}"
	#do
		#d_path=${d::-5}
		#cd $d_path
		#echo 
		#echo $d_path
		#echo 
		#echo '#pull from origin'
		#echo 
		#env -i git pull origin
		#echo 
		#echo '#push on gitlab'
		#echo 
		#env -i git push gitlab
		##sleep '$Timeout'
	#done
	#timeout_monitor
#}
#
#timeout_monitor
