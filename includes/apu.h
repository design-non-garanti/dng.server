/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * apu.h is generated from apu.h.in by configure -- do not edit apu.h
 */
/* @file apu.h
 * @brief APR-Utility main file
 */
#ifndef APU_H
#define APU_H

#if defined(DOXYGEN) || !defined(WIN32)

#define APU_DECLARE(type)            type

#define APU_DECLARE_NONSTD(type)     type

#define APU_DECLARE_DATA
#elif defined(APU_DECLARE_STATIC)
#define APU_DECLARE(type)            type __stdcall
#define APU_DECLARE_NONSTD(type)     type __cdecl
#define APU_DECLARE_DATA
#elif defined(APU_DECLARE_EXPORT)
#define APU_DECLARE(type)            __declspec(dllexport) type __stdcall
#define APU_DECLARE_NONSTD(type)     __declspec(dllexport) type __cdecl
#define APU_DECLARE_DATA             __declspec(dllexport)
#else
#define APU_DECLARE(type)            __declspec(dllimport) type __stdcall
#define APU_DECLARE_NONSTD(type)     __declspec(dllimport) type __cdecl
#define APU_DECLARE_DATA             __declspec(dllimport)
#endif

#if !defined(WIN32) || defined(APU_MODULE_DECLARE_STATIC)

#define APU_MODULE_DECLARE_DATA
#else
#define APU_MODULE_DECLARE_DATA           __declspec(dllexport)
#endif

/*
 * we always have SDBM (it's in our codebase)
 */
#define APU_HAVE_SDBM   1
#define APU_HAVE_GDBM   0
#define APU_HAVE_NDBM   0
#define APU_HAVE_DB     0

#if APU_HAVE_DB
#define APU_HAVE_DB_VERSION    0
#endif

#define APU_HAVE_PGSQL         1
#define APU_HAVE_MYSQL         0
#define APU_HAVE_SQLITE3       1
#define APU_HAVE_SQLITE2       0
#define APU_HAVE_ORACLE        0
#define APU_HAVE_FREETDS       0
#define APU_HAVE_ODBC          0

#define APU_HAVE_CRYPTO        1
#define APU_HAVE_OPENSSL       1
#define APU_HAVE_NSS           0

#define APU_HAVE_APR_ICONV     0
#define APU_HAVE_ICONV         1
#define APR_HAS_XLATE          (APU_HAVE_APR_ICONV || APU_HAVE_ICONV)

#endif /* APU_H */
